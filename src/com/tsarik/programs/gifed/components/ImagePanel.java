package com.tsarik.programs.gifed.components;

import java.awt.BorderLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.tsarik.components.ImageComponent;

public class ImagePanel extends JPanel {
	
	public ImagePanel() {
		imageComponent = new ImageComponent();
		header = new JLabel();
		footer = new JLabel();
		setHeaderString("");
		setFooterString("");
		
		JPanel p1 = new JPanel();
		p1.add(header);
		
		JPanel p2 = new JPanel();
		p2.setLayout(new BorderLayout());
		p2.add(imageComponent, BorderLayout.CENTER);
		
		JPanel p3 = new JPanel();
		p3.add(footer);
		
		setLayout(new BorderLayout());
		add(p2, BorderLayout.CENTER);
		add(p1, BorderLayout.NORTH);
		add(p3, BorderLayout.SOUTH);
	}
	
	public File getImageFile() {
		return f;
	}
	
	public void loadImage(File f) throws IOException {
		this.f = f;
		if (f == null) {
			imageComponent.setImage(null);
			return;
		}
		BufferedImage image = ImageIO.read(f);
		imageComponent.setImage(image);
		setHeaderString(f.getName());
	}
	
	public String getHeaderString() {
		return headerString;
	}
	
	public void setHeaderString(String s) {
		this.headerString = s;
		if (headerString.equals("")) {
			header.setText("<No image>");
		} else {
			header.setText(headerString);
		}
	}
	
	public String getFooterString() {
		return footerString;
	}
	
	public void setFooterString(String s) {
		this.footerString = s;
		footer.setText(s);
	}
	
	private File f;
	private ImageComponent imageComponent;
	private JLabel header;
	private JLabel footer;
	private String headerString;
	private String footerString;
	
}
