package com.tsarik.programs.gifed.actions;

import javax.swing.AbstractAction;

import com.tsarik.programs.gifed.frames.MainFrame;


public abstract class AbstractProgramMenuAction extends AbstractAction {
	
	public AbstractProgramMenuAction(String name) {
		super(name);
		this.owner = null;
	}
	
	public AbstractProgramMenuAction(MainFrame owner, String name) {
		super(name);
		this.owner = owner;
	}
	
	protected MainFrame owner;

}
