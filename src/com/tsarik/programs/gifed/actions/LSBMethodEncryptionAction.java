package com.tsarik.programs.gifed.actions;

import java.awt.event.ActionEvent;

import com.tsarik.programs.gifed.frames.MainFrame;
import com.tsarik.programs.gifed.gif.GIFEncryptorByLSBMethod;

public class LSBMethodEncryptionAction extends AbstractProgramMenuAction {
	
	public LSBMethodEncryptionAction(MainFrame owner) {
		super(owner, "LSB Method...");
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		owner.encryptImage(new GIFEncryptorByLSBMethod());
	}

}
