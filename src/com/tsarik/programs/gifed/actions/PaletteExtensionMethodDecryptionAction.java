package com.tsarik.programs.gifed.actions;

import java.awt.event.ActionEvent;

import com.tsarik.programs.gifed.frames.MainFrame;
import com.tsarik.programs.gifed.gif.GIFEncryptorByPaletteExtensionMethod;

public class PaletteExtensionMethodDecryptionAction extends AbstractProgramMenuAction {
	
	public PaletteExtensionMethodDecryptionAction(MainFrame owner) {
		super(owner, "Palette Extension Method");
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		owner.decryptImage(new GIFEncryptorByPaletteExtensionMethod());
	}

}
