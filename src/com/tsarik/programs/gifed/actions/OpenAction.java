package com.tsarik.programs.gifed.actions;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import com.tsarik.programs.gifed.frames.MainFrame;

public class OpenAction extends AbstractProgramMenuAction {
	
	public OpenAction(MainFrame owner) {
		super(owner, "Open...");
		putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK));
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		owner.openImage();
	}

}
