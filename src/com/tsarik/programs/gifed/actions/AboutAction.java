package com.tsarik.programs.gifed.actions;

import java.awt.event.ActionEvent;

import com.tsarik.programs.gifed.Parameters;
import com.tsarik.programs.gifed.frames.MainFrame;

public class AboutAction extends AbstractProgramMenuAction {
	
	public AboutAction(MainFrame owner) {
		super(owner, "About...");
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		Parameters.showAbout(owner);
	}

}
