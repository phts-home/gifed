package com.tsarik.programs.gifed;

import java.awt.Component;

import javax.swing.JOptionPane;

public class Parameters {
	
	public static final String PROGRAM_NAME = "GIFed";
	public static final String PROGRAM_VERSION = "1.0.1";
	public static final int PROGRAM_BUILD = 10;
	public static final int PROGRAM_YEAR = 2010;
	public static final String PROGRAM_AUTHOR = "Phil Tsarik";
	
	public static int MAINFRAME_WIDTH = 550;
	public static int MAINFRAME_HEIGHT = 550;
	
	public static String MESSAGE_ENCRYPTION_ERROR = "Encryption error occurred";
	public static String MESSAGE_DECRYPTION_ERROR = "Decryption error occurred";
	public static String MESSAGE_UNEXPECTED_ERROR = "Unexpected error occurred. Please contact developers";
	public static String MESSAGE_IO_ERROR = "I/O error occurred";
	
	public static String MESSAGE_ENCRYPTION_COMPLETED = "Encryption completed";
	public static String MESSAGE_DECRYPTION_COMPLETED = "Decryption completed. \n\nDecrypted text:";
	
	public static void showErrorMessage(Component parentComponent, String message, String errorMessage) {
		JOptionPane.showMessageDialog(parentComponent, message+"\n\nError message:\n\n"+"<html><textarea cols=20 rows=2>"+errorMessage.replaceAll("[\n\r]", " ")+"</textarea></html>", Parameters.PROGRAM_NAME, JOptionPane.ERROR_MESSAGE);
	}
	
	public static void showInformationMessage(Component parentComponent, String message) {
		JOptionPane.showMessageDialog(parentComponent, message, Parameters.PROGRAM_NAME, JOptionPane.INFORMATION_MESSAGE);
	}
	
	public static void showAbout(Component parentComponent) {
		JOptionPane.showMessageDialog(parentComponent,
				"<html><b>"
					+"<h3>"+Parameters.PROGRAM_NAME+"</h3>"
					+"<br>Version: "+Parameters.PROGRAM_VERSION+"."+Parameters.PROGRAM_BUILD+"</b></html>\n\n"
					+"<html>"
					+"&#169; "+Parameters.PROGRAM_YEAR+" by "+Parameters.PROGRAM_AUTHOR
					+"<br>&nbsp;</html>",
				"About", JOptionPane.INFORMATION_MESSAGE);
	}
	
}
