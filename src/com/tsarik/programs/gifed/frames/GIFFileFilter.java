package com.tsarik.programs.gifed.frames;

import java.io.File;

import javax.swing.filechooser.FileFilter;

class GIFFileFilter extends FileFilter {
	
	@Override
	public boolean accept(File f) {
		if (f.isDirectory()) {
			return true;
		}
		return isGifExtension(f);
	}
	
	@Override
	public String getDescription() {
		return "GIF Images";
	}
	
	public static boolean isGifExtension(File f) {
		String ext = null;
		String s = f.getName();
		int i = s.lastIndexOf('.');
		
		if (i > 0 &&  i < s.length() - 1) {
			ext = s.substring(i+1).toLowerCase();
		}
		
		if (ext != null) {
			if (ext.equalsIgnoreCase("gif")) {
				return true;
			}
		}
		return false;
	}
	
	public static File addExtension(File f) {
		if (!GIFFileFilter.isGifExtension(f)) {
			String n = f.getName().concat(".gif");
			f = new File(f.getParentFile(), n);
		}
		return f;
	}
	
}
