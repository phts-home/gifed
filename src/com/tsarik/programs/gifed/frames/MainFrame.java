package com.tsarik.programs.gifed.frames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;

import com.tsarik.programs.gifed.Parameters;
import com.tsarik.programs.gifed.actions.AboutAction;
import com.tsarik.programs.gifed.actions.ExitAction;
import com.tsarik.programs.gifed.actions.LSBMethodDecryptionAction;
import com.tsarik.programs.gifed.actions.LSBMethodEncryptionAction;
import com.tsarik.programs.gifed.actions.OpenAction;
import com.tsarik.programs.gifed.actions.PaletteExtensionMethodDecryptionAction;
import com.tsarik.programs.gifed.actions.PaletteExtensionMethodEncryptionAction;
import com.tsarik.programs.gifed.components.ImagePanel;
import com.tsarik.programs.gifed.gif.EncryptingFileParameters;
import com.tsarik.programs.gifed.gif.GIFEncryptorByLSBMethod;
import com.tsarik.programs.gifed.gif.GIFEncryptorByPaletteExtensionMethod;
import com.tsarik.steganography.Encryptor;
import com.tsarik.steganography.UnableToDecodeException;
import com.tsarik.steganography.UnableToEncodeException;


public class MainFrame extends JFrame {
	
	public MainFrame() {
		setSize(Parameters.MAINFRAME_WIDTH, Parameters.MAINFRAME_HEIGHT);
		Dimension scrSize = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(scrSize.width/2 - Parameters.MAINFRAME_WIDTH/2, scrSize.height/2 - Parameters.MAINFRAME_HEIGHT/2);
		setTitle(Parameters.PROGRAM_NAME);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		addWindowListener(new WindowEventsHandler());
		createComponents();
	}
	
	public void openImage() {
		if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			loadImageFile(fileChooser.getSelectedFile());
		}
	}
	
	public void encryptImage(Encryptor encryptor) {
		if (imagePanel.getImageFile() == null) {
			return;
		}
		String s = JOptionPane.showInputDialog(this, "Embedded text", Parameters.PROGRAM_NAME, JOptionPane.INFORMATION_MESSAGE);
		if (s == null) {
			return;
		}
		
		if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
			try {
				File out = GIFFileFilter.addExtension(fileChooser.getSelectedFile());
				encryptor.encrypt(imagePanel.getImageFile(), out, s);
				imagePanel.loadImage(out);
				Parameters.showInformationMessage(this, Parameters.MESSAGE_ENCRYPTION_COMPLETED);
			} catch (UnableToEncodeException e) {
				Parameters.showErrorMessage(this, Parameters.MESSAGE_ENCRYPTION_ERROR, e.getMessage());
			} catch (IOException e) {
				Parameters.showErrorMessage(this, Parameters.MESSAGE_IO_ERROR, e.getMessage());
			} catch (NullPointerException e) {
				Parameters.showErrorMessage(this, Parameters.MESSAGE_UNEXPECTED_ERROR, e.getMessage());
			}
		}
	}
	
	public void decryptImage(Encryptor encryptor) {
		if (imagePanel.getImageFile() == null) {
			return;
		}
		try {
			String s =  encryptor.decrypt(imagePanel.getImageFile());
			JOptionPane.showInputDialog(this, Parameters.MESSAGE_DECRYPTION_COMPLETED, Parameters.PROGRAM_NAME, JOptionPane.INFORMATION_MESSAGE, null, null, s);
		} catch (UnableToDecodeException e) {
			Parameters.showErrorMessage(this, Parameters.MESSAGE_DECRYPTION_ERROR, e.getMessage());
		} catch (IOException e) {
			Parameters.showErrorMessage(this, Parameters.MESSAGE_IO_ERROR, e.getMessage());
		} catch (NullPointerException e) {
			Parameters.showErrorMessage(this, Parameters.MESSAGE_UNEXPECTED_ERROR, e.getMessage());
		}
	}
	
	public void exit() {
		this.dispose();
	}
	
	private ImagePanel imagePanel;
	private JFileChooser fileChooser;
	
	private void createComponents() {
		JMenu fileMenu = new JMenu("File");
		fileMenu.add(new OpenAction(this));
		fileMenu.addSeparator();
		fileMenu.add(new ExitAction(this));
		
		JMenu encryptionMenu = new JMenu("Encryption");
		encryptionMenu.add(new LSBMethodEncryptionAction(this));
		encryptionMenu.add(new PaletteExtensionMethodEncryptionAction(this));
		
		JMenu decryptionMenu = new JMenu("Decryption");
		decryptionMenu.add(new LSBMethodDecryptionAction(this));
		decryptionMenu.add(new PaletteExtensionMethodDecryptionAction(this));
		
		JMenu helpMenu = new JMenu("Help");
		helpMenu.add(new AboutAction(this));
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(fileMenu);
		menuBar.add(encryptionMenu);
		menuBar.add(decryptionMenu);
		menuBar.add(helpMenu);
		setJMenuBar(menuBar);
		
		fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File("."));
		fileChooser.addChoosableFileFilter(new GIFFileFilter());
		fileChooser.setAcceptAllFileFilterUsed(false);
		fileChooser.setMultiSelectionEnabled(false);
		
		imagePanel = new ImagePanel();
		
		this.add(imagePanel, BorderLayout.CENTER);
		
	}
	
	private void loadImageFile(File f) {
		try {
			imagePanel.loadImage(f);
			EncryptingFileParameters p1 = GIFEncryptorByLSBMethod.getEncryptingFileParameters(f);
			EncryptingFileParameters p2 = GIFEncryptorByPaletteExtensionMethod.getEncryptingFileParameters(f);
			imagePanel.setFooterString("<html><center>File size: " + f.length() + " bytes<br>Possible text length (LSB Method): " + p1.getPossibleTextLength() + 
					"<br>Possible text length (Palette Extension Method): " + p2.getPossibleTextLength() + "</center></html>");
		} catch (UnableToEncodeException e) {
			Parameters.showErrorMessage(this, Parameters.MESSAGE_ENCRYPTION_ERROR, e.getMessage());
		} catch (IOException e) {
			Parameters.showErrorMessage(this, Parameters.MESSAGE_IO_ERROR, e.getMessage());
		} catch (Exception e) {
			Parameters.showErrorMessage(this, Parameters.MESSAGE_UNEXPECTED_ERROR, e.getMessage());
		}
		
	}
	
	
	private class WindowEventsHandler extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			MainFrame.this.exit();
		}
	}
	
	
}
