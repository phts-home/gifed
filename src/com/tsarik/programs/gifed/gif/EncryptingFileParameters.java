package com.tsarik.programs.gifed.gif;



public class EncryptingFileParameters {
	
	public EncryptingFileParameters(int possibleTextLength) {
		this.possibleTextLength = possibleTextLength;
	}
	
	public int getPossibleTextLength() {
		return possibleTextLength;
	}
	
	private int possibleTextLength;
	
}
